package com.sqlitedb;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

import java.util.ArrayList;

/**
 * Created by Acer on 10/12/2015.
 */
public class BAL {
    DBConnection dbConnection;

    public BAL(Context context) {
        dbConnection = new DBConnection(context);
    }

    public long insert(StudentBeen studentBeen){
        SQLiteDatabase sqLiteDatabase = dbConnection.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("StudentName",studentBeen.getStudentName());
        contentValues.put("StudentAge", studentBeen.getStudentAge());
        sqLiteDatabase.insert("Students", null, contentValues);
        sqLiteDatabase.close();
        return (long)0;
    }

    public void delete(int SID){
        SQLiteDatabase sqLiteDatabase = dbConnection.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        sqLiteDatabase.delete("Students", "SID=?", new String[]{String.valueOf(SID)});
        sqLiteDatabase.close();

    }

    public ArrayList<String> getAllData(){
        ArrayList<String> arrayList = new ArrayList<String>();
        SQLiteDatabase sqLiteDatabase = dbConnection.getWritableDatabase();
        String query = "SELECT * FROM Students";
        Cursor cursor = sqLiteDatabase.rawQuery(query,null);
        if(cursor.getCount()>0){
            while (cursor.moveToNext()){
                StudentBeen been = new StudentBeen();
                been.setSID(cursor.getInt(0));
                been.setStudentName(cursor.getString(1));
                been.setStudentAge(cursor.getInt(2));
                arrayList.add(been.getStudentName()+ "\t\t"+been.getStudentAge());
            }
        }

        sqLiteDatabase.close();
        return arrayList;
    }
}
