package com.sqlitedb;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Acer on 10/12/2015.
 */
public class DBConnection extends SQLiteOpenHelper {

    public final static String DBname = "SQLiteDB";
    public final static int db_version = 1;

    public DBConnection(Context context) {
        super(context, DBname, null, db_version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
      String table = "CREATE TABLE IF NOT EXISTS Students(SID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
              "StudentName TEXT,StudentAge INTEGER)";
        sqLiteDatabase.execSQL(table);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
