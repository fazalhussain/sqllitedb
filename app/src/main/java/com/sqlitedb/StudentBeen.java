package com.sqlitedb;

/**
 * Created by Acer on 10/12/2015.
 */
public class StudentBeen {
    private int SID;
    private String StudentName;
    private int StudentAge;

    public int getSID() {
        return SID;
    }

    public void setSID(int SID) {
        this.SID = SID;
    }

    public String getStudentName() {
        return StudentName;
    }

    public void setStudentName(String studentName) {
        StudentName = studentName;
    }

    public int getStudentAge() {
        return StudentAge;
    }

    public void setStudentAge(int studentAge) {
        StudentAge = studentAge;
    }
}
