package com.sqlitedb;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<String> arrayList;
    BAL bal;
    ListView lv;
    EditText studentname,studentage;
    Button submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        arrayList = new ArrayList<String>();
        bal = new BAL(this);
        lv = (ListView) findViewById(R.id.listView);
        studentname = (EditText) findViewById(R.id.editTextstudentname);
        studentage = (EditText) findViewById(R.id.editTextstudentage);
        show();

        ((Button) findViewById(R.id.buttonsubmit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StudentBeen been = new StudentBeen();
                been.setStudentName(studentname.getText().toString());
                been.setStudentAge(Integer.valueOf(studentage.getText().toString()));
                bal.insert(been);
                studentage.setText("");
                studentname.setText("");
                show();
            }
        });

        ((ListView) findViewById(R.id.listView)).setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                bal.delete(i);
                show();
                Toast.makeText(getApplicationContext(),"Delete Success",Toast.LENGTH_LONG).show();
                return true;
            }
        });
    }


    public void show(){
        arrayList = bal.getAllData();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_list_item_1,arrayList);
        lv.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }

}
